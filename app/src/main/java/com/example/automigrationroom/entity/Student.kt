package com.example.automigrationroom.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "student_table")
data class Student(
    val name: String,
    val subject: String,
    val dob: String,
    @PrimaryKey(autoGenerate = false) val id: Int?=null,
)