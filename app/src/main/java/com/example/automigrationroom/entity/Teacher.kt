package com.example.automigrationroom.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "teacher_table")
data class Teacher(
    val name: String,
    val qualification: String,
    val dob: String,
    @PrimaryKey(autoGenerate = false) val id: Int?=null,
)