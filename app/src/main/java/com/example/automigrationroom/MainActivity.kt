package com.example.automigrationroom

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.example.automigrationroom.entity.Teacher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val db = CollegeDB.getInstance(this)
        findViewById<Button>(R.id.buttonEntry).setOnClickListener {
            populateDatabase(db)
        }
        findViewById<Button>(R.id.buttonCount).setOnClickListener {
            val teachers: LiveData<List<Teacher>> = db.teacherDao().getAllTeachers()
            teachers.observe(this, Observer {
                Toast.makeText(this, "size of list ${it.size}", Toast.LENGTH_SHORT).show()
            })
        }
    }

   private fun populateDatabase(db: CollegeDB) {
        val teacherDao = db.teacherDao()
        GlobalScope.launch {
            teacherDao.insert(Teacher("Akhilesh", "M.S.C", "12/10/1996",))
           // customerDao.insert(Customer("Amit", "desc 2", "01/09/1991"))
           // customerDao.insert(Customer("Vijay", "desc 3", "15/08/1989"))
        }
    }
}