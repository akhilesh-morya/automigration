package com.example.automigrationroom.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.automigrationroom.entity.Student

@Dao
interface StudentDao {

    @Insert
    fun insert(student: Student)

    @Update
    fun update(student: Student)

    @Delete
    fun delete(student: Student)

    @Query("delete from student_table")
    fun deleteAllStudents()

    @Query("select * from student_table")
    fun getAllStudents(): LiveData<List<Student>>
}