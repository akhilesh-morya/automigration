package com.example.automigrationroom.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.automigrationroom.entity.Teacher

@Dao
interface TeacherDao {

    @Insert
    fun insert(teacher: Teacher)

    @Update
    fun update(teacher: Teacher)

    @Delete
    fun delete(teacher: Teacher)

    @Query("delete from teacher_table")
    fun deleteAllTeachers()

   // @Query("select * from customer_table order by age desc")
    @Query("select * from teacher_table")
    fun getAllTeachers(): LiveData<List<Teacher>>
}