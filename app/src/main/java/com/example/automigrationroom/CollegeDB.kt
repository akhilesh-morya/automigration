package com.example.automigrationroom

import com.example.automigrationroom.dao.TeacherDao
import com.example.automigrationroom.entity.Teacher
import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.migration.AutoMigrationSpec
import com.example.automigrationroom.dao.StudentDao
import com.example.automigrationroom.entity.Student


@Database(
    entities = [Teacher::class,Student::class],
    version = 1,
        //  exportSchema = true,
        //  autoMigrations = [
        //  AutoMigration(from = 2, to = 3, spec = CustomerDatabase.MySpec::class)
        //  AutoMigration(from = 1, to = 2)
        //  ],
)

abstract class CollegeDB : RoomDatabase() {

    abstract fun teacherDao(): TeacherDao
    abstract fun studentDao(): StudentDao

    @RenameColumn(
        tableName = "student_table",
        fromColumnName = "subject",
        toColumnName = "topic"
    )

    class MySpec : AutoMigrationSpec{
        override fun onPostMigrate(db: SupportSQLiteDatabase) {
            super.onPostMigrate(db)
        }
    }


    companion object {
        private var instance: CollegeDB? = null

        @Synchronized
        fun getInstance(ctx: Context): CollegeDB {
            if (instance == null)
                instance = Room.databaseBuilder(
                    ctx.applicationContext, CollegeDB::class.java,
                    "college_database"
                )
                    .build()
            return instance!!

        }

        private val roomCallback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                //populateDatabase(instance!!)
            }
        }
    }


}